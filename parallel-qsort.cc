/**
 *  \file parallel-qsort.cc
 *
 *  \brief Implement your parallel quicksort algorithm in this file.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "sort.hh"
#include <cilk/cilk.h>
#define TUNING_FACTOR 8
/**
 *  Given a pivot value, this routine partitions a given input array
 *  into two sets: the set A_le, which consists of all elements less
 *  than or equal to the pivot, and the set A_gt, which consists of
 *  all elements strictly greater than the pivot.
 *
 *  This routine overwrites the original input array with the
 *  partitioned output. It also returns the index n_le such that
 *  (A[0:(k-1)] == A_le) and (A[k:(N-1)] == A_gt).
 */

int parallel_part_factor;
int partition (keytype pivot, int N, keytype* A)
{
  int k = 0;
  for (int i = 0; i < N; ++i) {
    /* Invariant:
     * - A[0:(k-1)] <= pivot; and
     * - A[k:(i-1)] > pivot
     */
    const keytype ai = A[i];
    if (ai <= pivot) {
      /* Swap A[i] and A[k] */
      keytype ak = A[k];
      A[k++] = ai;
      A[i] = ak;
     }
   }
  return k;
}


int parallel_partition(keytype pivot, int N, keytype * A) {
    if (N < parallel_part_factor || N <= 2 * TUNING_FACTOR) {
	//sequential partition
        return partition(pivot, N, A);
    }
    int procs = TUNING_FACTOR;
    int pivs[TUNING_FACTOR];
    int mid2 = (TUNING_FACTOR -1) * N / TUNING_FACTOR;
   cilk_for(int i = 0; i < procs; i++) {
	int mid1 = i * N / procs;
	int mid2 = (i + 1) * N/procs;
	pivs[i]= partition(pivot,mid2 - mid1 ,A + mid1);
	}	
	
    return pivs[0] + partition(pivot, mid2 + pivs[procs -1] - pivs[0], A + pivs[0]);
}

void
quickSort (int N, keytype* A)
{
  const int G = 4000; /* base case size, a tuning parameter */
  if (N < G){ 
    sequentialSort (N, A);
}
  else {
    // Choose pivot at random
    keytype pivot = A[rand () % N];
    // Partition around the pivot. Upon completion, n_less, n_equal,
    // and n_greater should each be the number of keys less than,
    // equal to, or greater than the pivot, respectively. Moreover, the array
    int n_le = parallel_partition (pivot, N, A);
    _Cilk_spawn quickSort (n_le, A);
     quickSort (N-n_le, A + n_le);
    
  }
}

void
parallelSort (int N, keytype* A)
{
  parallel_part_factor = N;
  quickSort (N, A);
}

/* eof */
