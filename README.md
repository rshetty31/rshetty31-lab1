Name : Shetty Rajaram
Bitbucket ID: rshetty31
Email address: rshetty31@gatech.edu



Algorithm:

Parallel sequential-partitioning  with the same pivot is done over separate partitions of the input array.At the end of one parallel sweep,
we know for sure that the elements to the left of the left most pivot and the elements to the right of the right most pivot 
are already partitioned and can be excluded from further partitioning.
A sequential partition is then done in this range, and the  position of the pivot added to the offset due to savings is returned for further sorting.

The number of partitions to be done on the array can be pre configured, I found that the number 8 gave the optimal results. 
The minimum size of the array to be partitioned parallely is also configurabe, 
but I did observe that it becomes inefficient for smaller array lengths.

Performance :
A couple of sample output and error output files are included in the repository
I have seen the numbers reach 60Mkps, but the range is usually between 53MKps and 57 MKps. 
Once in a while the numbers for one or two trials in a batch go down to late 40s but I am suspecting the non chache-optimality of my code to be the culprit.


